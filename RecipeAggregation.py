import requests

# API credentials
edamam_app_id = '19df102b1'
edamam_app_key = '7946a5aae9c77783e48889ddccf1eca54'
spoonacular_api_key = '32e1702ec465e49ff83f708290fec9d44'

# Available recipe types
recipe_types = ['chicken', 'beef', 'vegetarian', 'pasta', 'dessert']

# Prompt user to select recipe type
print("Available recipe types:")
for i, recipe_type in enumerate(recipe_types):
    print(f"{i+1}. {recipe_type}")
selected_type = input("Select a recipe type (enter a number): ")
while not selected_type.isdigit() or int(selected_type) < 1 or int(selected_type) > len(recipe_types):
    selected_type = input("Invalid input. Please enter a valid number: ")
selected_type = recipe_types[int(selected_type) - 1]

# Generate recipe using Edamam API
edamam_url = f"https://api.edamam.com/search?q={selected_type}&app_id={edamam_app_id}&app_key={edamam_app_key}"
edamam_response = requests.get(edamam_url)
edamam_recipe = edamam_response.json()['hits'][0]['recipe']

# Generate recipe using Spoonacular API
spoonacular_url = f"https://api.spoonacular.com/recipes/search?query={selected_type}&number=1&apiKey={spoonacular_api_key}"
spoonacular_response = requests.get(spoonacular_url)
spoonacular_recipe_id = spoonacular_response.json()['results'][0]['id']

spoonacular_url2 = f"https://api.spoonacular.com/recipes/{spoonacular_recipe_id}/information?includeNutrition=false&apiKey={spoonacular_api_key}"
spoonacular_response2 = requests.get(spoonacular_url2)
spoonacular_recipe = spoonacular_response2.json()

# Output the recipe information to the user
print(f"Edamam Recipe: {edamam_recipe['label']}")
print()
print("Ingredients:")
for ingredient in edamam_recipe['ingredientLines']:
    print("- " + ingredient)
print()
print("Instructions:")
for instruction in edamam_recipe['healthLabels']:
    print("- " + instruction)
print()

print(f"Spoonacular Recipe: {spoonacular_recipe['title']}")
print()
print("Ingredients:")
for ingredient in spoonacular_recipe['extendedIngredients']:
    if 'originalString' in ingredient:
        print("- " + ingredient['originalString'])
    else:
        print("- " + ingredient['name'])
print()
print("Instructions:")
for step in spoonacular_recipe['analyzedInstructions'][0]['steps']:
    print(f"{step['number']}. {step['step']}")
print()
